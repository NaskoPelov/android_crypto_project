package com.example.apelo.coinsvision;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    // Create the dropdown menu
    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Create the options in the dropdown menu
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {

        switch (item.getItemId()){
            case R.id.Cryptocuransy:
                Intent cryproIntent = new Intent(this, Cryptocuransy.class);
                this.startActivity(cryproIntent);
                return true;
            case R.id.setAlarms:
                Intent alarmIntent = new Intent(this, setAlarms.class);
                this.startActivity(alarmIntent);
                return true;
            case R.id.statistics:
                Intent statIntent = new Intent(this, Statistics.class);
                this.startActivity(statIntent);
                return true;
            case R.id.about:
                Intent aboutIntetn = new Intent(this, About.class);
                this.startActivity(aboutIntetn);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
